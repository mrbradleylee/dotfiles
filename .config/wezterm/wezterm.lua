-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This will hold the configuration.
local config = wezterm.config_builder()

-- This is where you actually apply your config choices

-- ⭐ UI settings ⭐

config.color_scheme = "Catppuccin Macchiato"
config.hide_tab_bar_if_only_one_tab = true
config.font = wezterm.font({
	family = "Operator Mono Lig",
})
config.font_size = 13

config.native_macos_fullscreen_mode = true
config.initial_rows = 60
config.initial_cols = 180
wezterm.on("format-window-title", function()
	local zoomed = "🧙🪄✨ Y'er a wizard  ⚡📖"
	return zoomed
end)

-- return the configuration to wezterm
return config
