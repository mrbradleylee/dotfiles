-- for config options run h: bufferline-configuration
require('bufferline').setup {
  options = {
    themable = true,
    offsets = {
      {
        filetype = "neo-tree",
        text = function()
          return vim.fn.getcwd()
        end,
        highlight = "Directory",
        text_align = "left"
      },
    },
    color_icons = true,
    get_element_icon = function (element)
      local icon, hl = require('nvim-web-devicons').get_icon_by_filetype(element.filetype, { default = false })
      return icon, hl
    end,
    show_buffer_icons = true,
    separator_style = "none",
  }
}
