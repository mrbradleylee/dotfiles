-- set default termguicolors options

vim.o.termguicolors = true
vim.cmd [[ colorscheme tokyonight-moon ]] -- update lualine as well

