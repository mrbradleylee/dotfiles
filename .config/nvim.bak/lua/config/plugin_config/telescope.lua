require('telescope').setup {
  -- enable fzf sorting
  extensions = {
    fzf = {
      fuzzy = true,
      override_generic_sorter = true,
      override_file_sorter = true,
      case_mode = "smart_case",
    }
  },
}

require('telescope').load_extension('fzf') -- load fzf for working with telescope
local builtin = require('telescope.builtin') -- load builtin functions

vim.keymap.set('n', '<c-p>', builtin.oldfiles, {})
vim.keymap.set('n', '<Space><Space>', builtin.find_files, { desc = "find files" })
vim.keymap.set('n', '<Space>fb', builtin.buffers, { desc = "buffers" })
vim.keymap.set('n', '<Space>fg', builtin.live_grep, { desc = "grep" })
vim.keymap.set('n', '<Space>fh', builtin.help_tags, { desc = "help tags" })
vim.keymap.set('n', '<Space>fp', builtin.builtin, { desc = "builtins" })
