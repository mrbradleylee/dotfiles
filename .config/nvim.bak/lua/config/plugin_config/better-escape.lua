-- 'jj' or 'jk' to escape insert mode

require('better_escape').setup {
  event = "InsertCharPre",
  opts = { timeout = 300 }
}
