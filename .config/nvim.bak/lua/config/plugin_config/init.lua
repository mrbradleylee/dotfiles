-- pass config to modules

require("config.plugin_config.colorscheme")
require("config.plugin_config.oil")
require("config.plugin_config.lualine")
require("config.plugin_config.comment")
require("config.plugin_config.mini")
require("config.plugin_config.bufferline")
require("config.plugin_config.telescope")
require("config.plugin_config.better-escape")
require("config.plugin_config.treesitter")
require("config.plugin_config.lsp")
require("config.plugin_config.completions")
require("html-css"):setup() -- CSS intellisense style support must load after completions
