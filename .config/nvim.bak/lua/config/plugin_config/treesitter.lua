require'nvim-treesitter.configs'.setup {
  -- list specific parser names
  ensure_installed = { "lua", "python", "html", "css", "vim", "markdown", "markdown_inline", },

  -- install parsers synchronously (applies to above ensure_installed)
  sync_install = false,
  auto_install = true,
  highlight = {
    enable = true,
  },
}
