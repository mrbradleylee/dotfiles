require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'auto', -- explicit colortheme here if desired
  },
  sections = {
    lualine_a = {
      {
        'filename',
        path = 1,
      }
    }
  }
}
