-- completion config based on lsp in lsp.lua
local cmp = require('cmp') -- nvim-cmp

require('luasnip.loaders.from_vscode').lazy_load() -- for friendlysnippets and cmpluasnip

cmp.setup({
  mapping = cmp.mapping.preset.insert({
    ['<C-k>'] = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert },
    ['<C-j>'] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
    ['<Tab>'] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-o>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = false }),
  }),
  snippet = { -- configure required snippet engine
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },

  sources = cmp.config.sources{
    { name = 'nvim_lsp' }, --complete from nvim_lsp
    { name = 'luasnip' }, --complete from luasnip
    {
      name = 'html-css',
      option = {
        enable_on = {
          "html", "njk",
        },
        file_extensions = { "css", "sass", "less" }, -- set local filetypes for class list
        style_sheets = {
          -- remote style config eg bootstrap/tailwind
          -- 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css',
        }
      }
    },
  }, {
    { name = 'buffer' }, --complete from buffer
    { name = 'path' }, --complete from buffer
  },

  -- enable filename path for css config
  formatting = {
        format = function(entry, vim_item)
            if entry.source.name == "html-css" then
                vim_item.menu = entry.completion_item.menu
            end
            return vim_item
        end
    }
})

-- require('html-css'):setup()
