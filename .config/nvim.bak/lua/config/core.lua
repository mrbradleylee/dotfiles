-- core vim settings

-- map leader to space. execute before plugins
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- set vim options
vim.opt.backspace = '2'
vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.autoread = true
-- vim.opt.showtabline = 2

-- use spaces for tabs
vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2

-- line numbers
vim.wo.number = true
vim.wo.relativenumber = true
vim.wo.signcolumn = 'yes'
