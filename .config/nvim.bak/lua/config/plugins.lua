-- lazy.nvim plugin manager - install if not installed
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- define plugins
local plugins = {
  {
  'folke/tokyonight.nvim', --storm/night/moon/day
  -- 'catppuccin/nvim', -- -latte/frappe/macchiato/mocha (default)
  lazy = false,
  priority = 1000,
  },
  'stevearc/oil.nvim',
  {
    'nvim-neo-tree/neo-tree.nvim', -- TODO: setup close if last buffer
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons',
      'MunifTanjim/nui.nvim'
    },
  },
  'moll/vim-bbye',
  'nvim-tree/nvim-web-devicons',
  'akinsho/bufferline.nvim',
  'nvim-lualine/lualine.nvim',
  {
    'echasnovski/mini.indentscope', -- fancy indent line
    version = false
  },
  'max397574/better-escape.nvim',
  'numToStr/Comment.nvim',
  {
  "folke/flash.nvim",
  event = "VeryLazy",
  ---@type Flash.Config
  opts = {},
  -- stylua: ignore
  keys = {
      { "s", mode = { "n", "x", "o" }, function() require("flash").jump() end, desc = "Flash" },
      { "S", mode = { "n", "x", "o" }, function() require("flash").treesitter() end, desc = "Flash Treesitter" },
      { "r", mode = "o", function() require("flash").remote() end, desc = "Remote Flash" },
      { "R", mode = { "o", "x" }, function() require("flash").treesitter_search() end, desc = "Treesitter Search" },
      { "<c-s>", mode = { "c" }, function() require("flash").toggle() end, desc = "Toggle Flash Search" },
    },
  },
  'nvim-treesitter/nvim-treesitter',

  -- lsp > completion > snippets
  'williamboman/mason.nvim',
  'williamboman/mason-lspconfig.nvim',
  'neovim/nvim-lspconfig',
  'nvimdev/lspsaga.nvim',
  'hrsh7th/nvim-cmp',
  'hrsh7th/cmp-nvim-lsp', -- nvim_lsp completion source
  {
	"L3MON4D3/LuaSnip",
	-- follow latest release.
	version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
	-- install jsregexp (optional!).
	build = "make install_jsregexp"
  },
  'saadparwaiz1/cmp_luasnip', -- luasnip snippet completion source
  'rafamadriz/friendly-snippets',
  'hrsh7th/cmp-buffer', -- buffer completion source
  'hrsh7th/cmp-path', -- path completion source
  {
    'Jezda1337/nvim-html-css',
    dependencies = {
      'nvim-treesitter/nvim-treesitter',
      'nvim-lua/plenary.nvim'
    }
  },
  {
    'nvim-telescope/telescope.nvim',
    dependencies = {
       'nvim-lua/plenary.nvim',
       { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' }
       }
  },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = {},
  },
  {
  'git@gitlab.com:gitlab-org/editor-extensions/gitlab.vim.git',
  event = { 'BufReadPre', 'BufNewFile' }, -- Activate when a file is created/opened
  ft = { 'go', 'javascript', 'python', 'ruby' }, -- Activate when a supported filetype is open
  -- cond = function()
  --   return vim.env.GITLAB_TOKEN ~= nil and vim.env.GITLAB_TOKEN ~= '' -- Only activate if token is present in environment variable (remove to use interactive workflow)
  -- end,
  opts = {
    statusline = {
      enabled = true, -- Hook into the builtin statusline to indicate the status of the GitLab Duo Code Suggestions integration
    },
  },
  },
}

-- load plugins from plugins.lua or alternatively lua/plugins/init.lua
-- should be EOF
require("lazy").setup(plugins, opts)
