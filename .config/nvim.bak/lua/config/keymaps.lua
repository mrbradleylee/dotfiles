-- keymaps. Some plugin specific keymaps are in plugin_config/*.lua

-- file opts
vim.keymap.set('n', '<leader>w', ':w<CR>', { desc = "save" }) -- leader write
vim.keymap.set('n', '<leader>x', ':Bdelete<CR>', { desc = "close" }) -- leader write quitkeymap
vim.keymap.set('n', '<leader>q', ':Bdelete!<CR>', { desc = "force close" }) -- leader quit without saving
vim.keymap.set('n', '<leader>qa', ':qa!<CR>') -- leader quit without saving

-- ui toggles
vim.keymap.set('n', '<leader>h', ':nohlsearch<CR>', { desc = "clear search" }) -- clear search highlights
vim.keymap.set('n', '<leader>e', ':Neotree toggle<CR>', { desc = "toggle NeoTree" }) -- tree toggle
vim.keymap.set('n', '<leader>o', '<c-w>w', { desc = "toggle NeoTree focus" }) -- toggle tree focus, closes Oil too for now

-- bufferline
vim.keymap.set('n', '<leader>bc', ':Bdelete!<CR>', { desc = "force close" }) -- close current buffer
vim.keymap.set('n', '<leader>bb', ':BufferLinePick<CR>', { desc = "pick buffer" }) -- bufferline picker
vim.keymap.set('n', '<leader>bx', ':BufferLineCloseOthers<CR>', { desc = "close all other" }) -- bufferline close all except
vim.keymap.set('n', '<leader>bh', ':BufferLineCyclePrev<CR>', { desc = "previous" }) -- bufferline move to prev
vim.keymap.set('n', '<leader>bl', ':BufferLineCycleNext<CR>', { desc = "next" }) -- bufferline move to next

vim.api.nvim_set_keymap('i', '<S-Tab>', '<C-X><C-O>', { noremap = true }) -- shift tab for omnicomplete glab duo
vim.keymap.set('n', '<leader>l', ':GitLabCodeSuggestionsStart<CR>', { desc = "glab duo" })
