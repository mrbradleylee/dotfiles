-- disable all if vscode
if vim.g.vscode then
  -- do nothing
require("config.vscode")
else
-- require config/core|keymaps|plugins.lua along with plugin_config/init.lua

require("config.core")
require("config.keymaps")
require("config.plugins")
require("config.plugin_config")
end
